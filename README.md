java 19

mvn spring-boot:run - run spring boot app

mvn test jacoco:report - run test and create reports. Reports and code coverage are located in target/site/jacoco/index.html

http://localhost:8080/swagger-ui/index.html - open api documentation for web services
