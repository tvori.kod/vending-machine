package com.example.vendingmachine.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductRepository productRepository;

    @Override
    public ServiceStatus addProduct(Product product) {
        long count = productRepository.count();
        if (count < 10) {
            if (productRepository.findById(product.getId()).isEmpty()) {
                productRepository.save(product);
                return ServiceStatus.SUCCESSFUL;
            } else {
                return ServiceStatus.ENTITY_ALREADY_EXIST;
            }
        } else {
            return ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT;
        }
    }

    @Override
    public ServiceStatus updateProduct(Product product) {
        if (productRepository.findById(product.getId()).isEmpty()) {
            long count = productRepository.count();
            if (count < 10) {
                productRepository.save(product);
                return ServiceStatus.SUCCESSFUL;
            } else {
                return ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT;
            }
        } else {
            productRepository.save(product);
            return ServiceStatus.SUCCESSFUL;
        }
    }

    @Override
    public ServiceStatus deleteProductById(long id) {
        if (productRepository.findById(id).isPresent()) {
            productRepository.deleteById(id);
            return ServiceStatus.SUCCESSFUL;
        } else {
            return ServiceStatus.ENTITY_NOT_FOUND;
        }
    }

    @Override
    public long count() {
        return productRepository.count();
    }
}