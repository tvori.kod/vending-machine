package com.example.vendingmachine.service;

import com.example.vendingmachine.entity.Product;

public interface ProductService {

    long count();

    ServiceStatus addProduct(Product product);

    ServiceStatus updateProduct(Product product);

    ServiceStatus deleteProductById(long id);

}