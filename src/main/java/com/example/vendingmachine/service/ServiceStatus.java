package com.example.vendingmachine.service;

public enum ServiceStatus {
    SUCCESSFUL,
    ENTITY_NOT_FOUND,
    ENTITY_ALREADY_EXIST,
    ENTITY_COLLECTION_MAX_SIZE_LIMIT
}