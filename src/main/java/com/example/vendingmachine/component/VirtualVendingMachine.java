package com.example.vendingmachine.component;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.ApplicationScope;

import com.example.vendingmachine.entity.Coin;
import com.example.vendingmachine.entity.State;
import com.example.vendingmachine.repository.ProductRepository;

import lombok.Generated;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Component
@ApplicationScope
public class VirtualVendingMachine {

    @Autowired
    @Generated
    private ProductRepository productRepository;

    private State currentState = State.INIT;
    private BigDecimal currentAmount = new BigDecimal("0.00");
    private String currentOuptut = "Welcome";

    public void insertCoin(Coin coin) {
        currentState.insertCoin(coin, this);
    }

    public void reset() {
        currentState.reset(this);
    }

    public void buyProductById(long id) {
        currentState.buyProductById(id, productRepository, this);
    }
}