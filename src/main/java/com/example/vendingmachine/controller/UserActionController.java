package com.example.vendingmachine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.vendingmachine.component.VirtualVendingMachine;
import com.example.vendingmachine.entity.Coin;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;

@RestController
public class UserActionController {

    @Autowired
    VirtualVendingMachine virtualVendingMachine;

    @PostMapping("/user-actions/coins")
    @Operation(summary = "insert coin to the vending machine", description = "Accept only the coins: _10ST, _20ST, _50ST, _1LV, _2LV")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Inserted coin _20ST and current amount is: 2.20")
    })
    public String insertCoin(@Valid @NotEmpty @RequestBody Coin coin) {
        virtualVendingMachine.insertCoin(coin);
        return virtualVendingMachine.getCurrentOuptut();
    }

    @DeleteMapping("/user-actions/coins")
    @Operation(summary = "reset the vending machine", description = "If there are coins in the vending machine will be returned")
    public String reset() {
        virtualVendingMachine.reset();
        return virtualVendingMachine.getCurrentOuptut();
    }

    @GetMapping("/user-actions/products/{id}")
    @Operation(summary = "buy product", description = "Select product from the vending machine")
    public String buyProduct(@Valid @NotEmpty @PathVariable long id) {
        virtualVendingMachine.buyProductById(id);
        return virtualVendingMachine.getCurrentOuptut();
    }

}