package com.example.vendingmachine.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.service.ProductService;
import com.example.vendingmachine.service.ServiceStatus;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import jakarta.validation.Valid;
import jakarta.validation.constraints.NotEmpty;
import lombok.extern.slf4j.Slf4j;

@RestController
@Slf4j
public class ProductController {

    @Autowired
    ProductService productService;

    @PostMapping("/products")
    @Operation(summary = "add product to the vending machine", description = "Id, name and price are mandatory")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product was added"),
            @ApiResponse(responseCode = "422", description = "There is already product with the same id / The vending machine can store 10 product as maximum")
    })
    public ResponseEntity<String> addProduct(@Valid @RequestBody Product product) {
        log.info("addProduct:" + product.toString());
        ServiceStatus serviceStatus = productService.addProduct(product);
        ResponseEntity<String> responseEntity = null;
        switch (serviceStatus) {
            case SUCCESSFUL:
                responseEntity = ResponseEntity.status(HttpStatus.OK)
                        .body("Product was added");
                break;
            case ENTITY_ALREADY_EXIST:
                responseEntity = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body("There is already product with the same id");
                break;
            case ENTITY_COLLECTION_MAX_SIZE_LIMIT:
                responseEntity = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body("The vending machine can store 10 product as maximum");
                break;
            default:
                break;
        }
        return responseEntity;
    }

    @PutMapping("/products")
    @Operation(summary = "update product to the vending machine", description = "Id, name and price are mandatory")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product is updated"),
            @ApiResponse(responseCode = "422", description = "There is product with the specified id, but cannot be added since vending machine can store 10 product as maximum")
    })
    public ResponseEntity<String> updateProduct(@Valid @RequestBody Product product) {
        log.info("updateProduct:" + product.toString());
        ServiceStatus serviceStatus = productService.updateProduct(product);
        ResponseEntity<String> responseEntity = null;
        switch (serviceStatus) {
            case SUCCESSFUL:
                responseEntity = ResponseEntity.status(HttpStatus.OK).body("Product is updated");
                break;
            case ENTITY_COLLECTION_MAX_SIZE_LIMIT:
                responseEntity = ResponseEntity.status(HttpStatus.UNPROCESSABLE_ENTITY)
                        .body("There is product with the specified id, but cannot be added since vending machine can store 10 product as maximum");
                break;
            default:
                break;
        }
        return responseEntity;
    }

    @DeleteMapping("/products/{id}")
    @Operation(summary = "delete product from the vending machine", description = "Id is mandatory")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Product was deleted"),
            @ApiResponse(responseCode = "400", description = "Product id was not found")
    })
    public ResponseEntity<String> deleteProductById(@Valid @NotEmpty @PathVariable long id) {
        ServiceStatus serviceStatus = productService.deleteProductById(id);
        if (serviceStatus == ServiceStatus.SUCCESSFUL) {
            return ResponseEntity.status(HttpStatus.OK).body("Product was deleted");
        } else {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body("Product id was not found");
        }
    }

}