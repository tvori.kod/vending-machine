package com.example.vendingmachine.entity;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.Optional;

import com.example.vendingmachine.component.VirtualVendingMachine;
import com.example.vendingmachine.repository.ProductRepository;

public enum State {

    INIT {
        @Override
        public void insertCoin(Coin coin, VirtualVendingMachine machine) {
            machine.setCurrentAmount(machine.getCurrentAmount().add(coinAmount(coin)));
            machine.setCurrentOuptut("Inserted coin " + coin + " and current amount is: " +
                    toMoney(machine.getCurrentAmount()));
            machine.setCurrentState(WAIT);
        }

        @Override
        public void reset(VirtualVendingMachine machine) {
            machine.setCurrentOuptut("No coins to be returned");
        }

        @Override
        public void buyProductById(long id, ProductRepository productRepository, VirtualVendingMachine machine) {
            machine.setCurrentOuptut("Please insert coins in order to buy product");
        }
    },
    WAIT {
        @Override
        public void insertCoin(Coin coin, VirtualVendingMachine machine) {
            machine.setCurrentAmount(machine.getCurrentAmount().add(coinAmount(coin)));
            machine.setCurrentOuptut("Inserted coin " + coin + " and current amount is: "
                    + toMoney(machine.getCurrentAmount()));
            machine.setCurrentState(WAIT);
        }

        @Override
        public void reset(VirtualVendingMachine machine) {
            machine.setCurrentOuptut("Amount to be returned: " +
                    toMoney(machine.getCurrentAmount()));
            machine.setCurrentAmount(new BigDecimal("0.00"));
            machine.setCurrentState(INIT);
        }

        @Override
        public void buyProductById(long id, ProductRepository productRepository, VirtualVendingMachine machine) {
            Optional<Product> product = productRepository.findById(id);
            if (product.isPresent()) {
                if (machine.getCurrentAmount().compareTo(product.get().getPrice()) >= 0) {
                    machine.setCurrentOuptut("Product was purchased. Thank you!");
                    machine.setCurrentAmount(new BigDecimal("0.00"));
                    machine.getProductRepository().deleteById(product.get().getId());
                    machine.setCurrentState(INIT);
                } else {
                    machine.setCurrentOuptut(
                            "Please insert more coins. You need to insert: " + toMoney(product.get().getPrice()
                                    .subtract(machine.getCurrentAmount())));
                }
            } else {
                machine.setCurrentOuptut("The requested product is not awailable please select other product");
            }

        }
    };

    public abstract void insertCoin(Coin coin, VirtualVendingMachine machine);

    public abstract void reset(VirtualVendingMachine machine);

    public abstract void buyProductById(long id, ProductRepository productRepository, VirtualVendingMachine machine);

    private final static BigDecimal coinAmount(Coin coin) {
        BigDecimal amount = null;
        switch (coin) {
            case _10ST:
                amount = new BigDecimal(0.10);
                break;
            case _1LV:
                amount = new BigDecimal(1.00);
                break;
            case _20ST:
                amount = new BigDecimal(0.20);
                break;
            case _2LV:
                amount = new BigDecimal(2.00);
                break;
            case _50ST:
                amount = new BigDecimal(0.50);
                break;
            default:
                break;
        }
        return amount;

    }

    public final static String toMoney(BigDecimal bd) {
        DecimalFormat df = new DecimalFormat("##0.00");
        return df.format(bd);
    }

}