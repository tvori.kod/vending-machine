package com.example.vendingmachine.entity;

public enum Coin {
    _10ST,
    _20ST,
    _50ST,
    _1LV,
    _2LV
}