package com.example.vendingmachine.entity;

import java.math.BigDecimal;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.validation.constraints.DecimalMax;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.Digits;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Generated;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Generated
public class Product {

    @Id
    @NotNull
    private Long id;
    @NotBlank(message = "name is mandatory")
    private String name;
    @NotNull(message = "price is mandatory")
    @DecimalMin("0.10")
    @DecimalMax("999.99")
    @Digits(integer = 3, fraction = 2, message = "price can be between 0.10 and 999.99")
    private BigDecimal price;
}