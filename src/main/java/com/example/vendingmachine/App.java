package com.example.vendingmachine;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import lombok.Generated;

@SpringBootApplication
public class App {

	@Generated
	public static void main(String[] args) {
		SpringApplication.run(App.class, args);
	}

}
