package com.example.vendingmachine;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;

import java.math.BigDecimal;
import java.util.Arrays;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.repository.ProductRepository;
import com.example.vendingmachine.service.ProductService;
import com.example.vendingmachine.service.ServiceStatus;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
class ProductServiceJUnit5Tests {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    private Product product;

    @BeforeEach
    public void setup() {
        product = Product.builder()
                .id(1L)
                .name("Water")
                .price(new BigDecimal("0.50"))
                .build();
    }

    @Test
    @Order(1)
    void testAddProductSuccessful() {
        assumeTrue(productRepository.findById(product.getId()).isEmpty());
        ServiceStatus serviceStatus = productService.addProduct(product);
        assertTrue(serviceStatus.equals(ServiceStatus.SUCCESSFUL));
    }

    @Test
    @Order(2)
    void testAddProductAlreadyExists() {
        assumeTrue(productRepository.findById(product.getId()).isPresent());
        ServiceStatus serviceStatus = productService.addProduct(product);
        assertTrue(serviceStatus.equals(ServiceStatus.ENTITY_ALREADY_EXIST));
    }

    @Test
    @Order(3)
    void testAddProductMaxSizeLimit() {
        Arrays.asList(2L, 3L, 4L, 5L, 6L, 7L, 8L, 9L, 10L).forEach(id -> {
            productService.addProduct(
                    Product.builder()
                            .id(id)
                            .name("Water")
                            .price(new BigDecimal("0.50"))
                            .build());
        });
        assumeTrue(productRepository.count() >= 10);
        ServiceStatus serviceStatus = productService.addProduct(product);
        assertTrue(serviceStatus.equals(ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT));
    }

    @Test
    @Order(4)
    void testCount() {
        assertTrue(productService.count() == 10);
    }

    @Test
    @Order(5)
    void deleteProductById() {
        assumeTrue(productRepository.findById(12L).isEmpty());
        assertTrue(productService.deleteProductById(12).equals(ServiceStatus.ENTITY_NOT_FOUND));
        assumeTrue(productRepository.findById(10L).isPresent());
        assertTrue(productService.deleteProductById(10).equals(ServiceStatus.SUCCESSFUL));
        assertTrue(productService.count() == 9);
    }

    @Test
    @Order(6)
    void updateProduct() {
        assumeTrue(productRepository.findById(9L).isPresent());
        productService.updateProduct(
                Product.builder()
                        .id(9L)
                        .name("Tea")
                        .price(new BigDecimal("0.50"))
                        .build());
        assumeTrue(productRepository.findById(9L).get().getName().equals("Tea"));
        assumeTrue(productRepository.findById(10L).isEmpty());
        assertTrue(productService.count() == 9);
        assertTrue(productService.updateProduct(
                Product.builder()
                        .id(10L)
                        .name("Orange juice")
                        .price(new BigDecimal("1.50"))
                        .build())
                .equals(ServiceStatus.SUCCESSFUL));
        assertTrue(productService.count() == 10);
        assumeTrue(productRepository.findById(10L).isPresent());
        assumeTrue(productRepository.findById(11L).isEmpty());
        assertTrue(productService.updateProduct(
                Product.builder()
                        .id(11L)
                        .name("Orange juice")
                        .price(new BigDecimal("1.50"))
                        .build())
                .equals(ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT));

    }

}