package com.example.vendingmachine;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import java.math.BigDecimal;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.vendingmachine.component.VirtualVendingMachine;
import com.example.vendingmachine.entity.Coin;
import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.entity.State;
import com.example.vendingmachine.service.ProductService;

@SpringBootTest
@TestMethodOrder(OrderAnnotation.class)
public class VirtualVendingMachineJUnit5Tests {

    @Autowired
    VirtualVendingMachine virtualVendingMachine;

    @Autowired
    private ProductService productService;

    private Product product;

    @BeforeEach
    public void setup() {
        product = Product.builder()
                .id(1L)
                .name("Water")
                .price(new BigDecimal("0.50"))
                .build();
        // product.toString();
        productService.updateProduct(product);
    }

    @Test
    @Order(1)
    void testInsertCoin() {
        assumeTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assumeTrue(virtualVendingMachine.getCurrentAmount().compareTo(new BigDecimal("0.00")) == 0);
        virtualVendingMachine.insertCoin(Coin._10ST);
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.WAIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.10"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("Inserted coin _10ST and current amount is: 0.10"));
        virtualVendingMachine.insertCoin(Coin._2LV);
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.WAIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("2.10"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("Inserted coin _2LV and current amount is: 2.10"));
    }

    @Test
    @Order(2)
    void testReset() {
        assumeTrue(virtualVendingMachine.getCurrentState().equals(State.WAIT));
        assumeTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("2.10"));
        virtualVendingMachine.reset();
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.00"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("Amount to be returned: 2.10"));
        virtualVendingMachine.reset();
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.00"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("No coins to be returned"));
    }

    @Test
    @Order(3)
    void buyProductById() {
        assumeTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assumeTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.00"));
        virtualVendingMachine.buyProductById(product.getId());
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.00"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("Please insert coins in order to buy product"));
        virtualVendingMachine.insertCoin(Coin._20ST);
        virtualVendingMachine.buyProductById(product.getId());
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.WAIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.20"));
        assertTrue(
                virtualVendingMachine.getCurrentOuptut().equals("Please insert more coins. You need to insert: 0.30"));
        virtualVendingMachine.insertCoin(Coin._10ST);
        virtualVendingMachine.insertCoin(Coin._10ST);
        virtualVendingMachine.insertCoin(Coin._10ST);
        virtualVendingMachine.buyProductById(product.getId());
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.INIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("0.00"));
        assertTrue(virtualVendingMachine.getCurrentOuptut().equals("Product was purchased. Thank you!"));
        virtualVendingMachine.insertCoin(Coin._1LV);
        virtualVendingMachine.insertCoin(Coin._50ST);
        ;
        virtualVendingMachine.buyProductById(100L);
        assertTrue(virtualVendingMachine.getCurrentState().equals(State.WAIT));
        assertTrue(State.toMoney(virtualVendingMachine.getCurrentAmount()).equals("1.50"));
        assertTrue(virtualVendingMachine.getCurrentOuptut()
                .equals("The requested product is not awailable please select other product"));
    }

}