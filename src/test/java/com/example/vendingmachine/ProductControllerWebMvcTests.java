package com.example.vendingmachine;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import java.math.BigDecimal;
import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import com.example.vendingmachine.controller.ProductController;
import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.service.ProductService;
import com.example.vendingmachine.service.ServiceStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(ProductController.class)
@TestMethodOrder(OrderAnnotation.class)
public class ProductControllerWebMvcTests {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    ProductService productService;

    @Autowired
    private ObjectMapper om;

    private static Product product = Product.builder()
            .id(1L)
            .name("Water")
            .price(new BigDecimal("0.50"))
            .build();

    @Test
    @Order(1)
    void addProductSUCCESSFUL() throws Exception {
        when(productService.addProduct(product)).thenReturn(ServiceStatus.SUCCESSFUL);
        mockMvc.perform(post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(product)))
                        .andExpect(status().isOk())
                        .andExpect(content().string("Product was added"));
    }

    @Test
    @Order(2)
    void addProductENTITY_ALREADY_EXIST() throws Exception {
        when(productService.addProduct(product)).thenReturn(ServiceStatus.ENTITY_ALREADY_EXIST);
        mockMvc.perform(post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(product)))
                        .andExpect(status().isUnprocessableEntity())
                        .andExpect(content().string("There is already product with the same id"));
    }

    @Test
    @Order(3)
    void addProductENTITY_COLLECTION_MAX_SIZE_LIMIT() throws Exception {
        when(productService.addProduct(product)).thenReturn(ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT);
        mockMvc.perform(post("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(product)))
                        .andExpect(status().isUnprocessableEntity())
                        .andExpect(content().string("The vending machine can store 10 product as maximum"));
    }

    @Test
    @Order(4)
    void updateProductSUCCESSFUL() throws Exception {
        when(productService.updateProduct(product)).thenReturn(ServiceStatus.SUCCESSFUL);
        mockMvc.perform(put("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(product)))
                        .andExpect(status().isOk())
                        .andExpect(content().string("Product is updated"));
    }

    @Test
    @Order(5)
    void updateProductENTITY_COLLECTION_MAX_SIZE_LIMIT() throws Exception {
        when(productService.updateProduct(product)).thenReturn(ServiceStatus.ENTITY_COLLECTION_MAX_SIZE_LIMIT);
        mockMvc.perform(put("/products")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(om.writeValueAsString(product)))
                        .andExpect(status().isUnprocessableEntity())
                        .andExpect(content().string("There is product with the specified id, but cannot be added since vending machine can store 10 product as maximum"));
    }

    @Test
    @Order(6)
    void deleteProductSUCCESSFUL() throws Exception {
        when(productService.deleteProductById(product.getId())).thenReturn(ServiceStatus.SUCCESSFUL);
        mockMvc.perform(delete("/products/{id}", product.getId()))
                        .andExpect(status().isOk())
                        .andExpect(content().string("Product was deleted"));
    }

    @Test
    @Order(7)
    void deleteProductENTITY_NOT_FOUND() throws Exception {
        when(productService.deleteProductById(product.getId())).thenReturn(ServiceStatus.ENTITY_NOT_FOUND);
        mockMvc.perform(delete("/products/{id}", product.getId().toString()))
                        .andExpect(status().isBadRequest())
                        .andExpect(content().string("Product id was not found"));
    }

}