package com.example.vendingmachine;

import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assumptions.assumeTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import java.math.BigDecimal;

import org.junit.jupiter.api.MethodOrderer.OrderAnnotation;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestMethodOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.example.vendingmachine.component.VirtualVendingMachine;
import com.example.vendingmachine.entity.Coin;
import com.example.vendingmachine.entity.Product;
import com.example.vendingmachine.repository.ProductRepository;
import com.example.vendingmachine.service.ProductService;
import com.example.vendingmachine.service.ServiceStatus;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@AutoConfigureMockMvc
@TestMethodOrder(OrderAnnotation.class)
public class UserActionControllerSpringBootTests {

    @Autowired
    VirtualVendingMachine virtualVendingMachine;

    @Autowired
    ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper om;

    private static Product product = Product.builder()
            .id(1L)
            .name("Water")
            .price(new BigDecimal("0.50"))
            .build();

    @Test
    @Order(1)
    void testAddProductSuccessful() {
        assumeTrue(productRepository.findById(product.getId()).isEmpty());
        ServiceStatus serviceStatus = productService.addProduct(product);
        assertTrue(serviceStatus.equals(ServiceStatus.SUCCESSFUL));
    }

    @Test
    @Order(2)
    void insertCoin() throws Exception {
        mockMvc.perform(post("/user-actions/coins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(Coin._50ST)))
                .andExpect(status().isOk())
                .andExpect(content().string("Inserted coin _50ST and current amount is: 0.50"));

    }

    @Test
    @Order(3)
    void reset() throws Exception {
        mockMvc.perform(delete("/user-actions/coins"))
                .andExpect(status().isOk())
                .andExpect(content().string("Amount to be returned: 0.50"));

    }

    @Test
    @Order(4)
    void buyProductById() throws Exception {
        mockMvc.perform(get("/user-actions/products/{id}", product.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string("Please insert coins in order to buy product"));

    }

    @Test
    @Order(5)
    void insertCoin_10ST() throws Exception {
        mockMvc.perform(post("/user-actions/coins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(Coin._10ST)))
                .andExpect(status().isOk())
                .andExpect(content().string("Inserted coin _10ST and current amount is: 0.10"));

    }

    @Test
    @Order(6)
    void buyProductByIdMissingMoney() throws Exception {
        mockMvc.perform(get("/user-actions/products/{id}", product.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string("Please insert more coins. You need to insert: 0.40"));

    }

    @Test
    @Order(7)
    void insertCoin_1LV() throws Exception {
        mockMvc.perform(post("/user-actions/coins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(Coin._1LV)))
                .andExpect(status().isOk())
                .andExpect(content().string("Inserted coin _1LV and current amount is: 1.10"));
    }

    @Test
    @Order(8)
    void buyProductByIdSuccess() throws Exception {
        mockMvc.perform(get("/user-actions/products/{id}", product.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string("Product was purchased. Thank you!"));

    }

    @Test
    @Order(9)
    void insertCoin_2LV() throws Exception {
        mockMvc.perform(post("/user-actions/coins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(Coin._2LV)))
                .andExpect(status().isOk())
                .andExpect(content().string("Inserted coin _2LV and current amount is: 2.00"));

    }

    @Test
    @Order(10)
    void buyProductByIdNotAvailable() throws Exception {
        mockMvc.perform(get("/user-actions/products/{id}", product.getId()))
                .andExpect(status().isOk())
                .andExpect(content().string("The requested product is not awailable please select other product"));

    }

    @Test
    @Order(9)
    void insertCoin_20ST() throws Exception {
        mockMvc.perform(post("/user-actions/coins")
                .contentType(MediaType.APPLICATION_JSON)
                .content(om.writeValueAsString(Coin._20ST)))
                .andExpect(status().isOk())
                .andExpect(content().string("Inserted coin _20ST and current amount is: 2.20"));

    }

}